//package com.epam.secondsort;
//
//import org.apache.hadoop.io.NullWritable;
//import org.junit.Test;
//
//import static org.hamcrest.CoreMatchers.is;
//import static org.junit.Assert.assertThat;
//
///**
// * User: Ihor Kovalyk
// * Date: 9/12/19
// * Time: 10:25 AM
// */
//
//public class NaturalKeyPartitionerTest {
//    @Test
//    public void testGetPartition() throws Exception {
//        Long hotelId = 282608848L;
//        String srchCI = "2017-09-19";
//        Long id = 17L;
//        CompositeKey compositeKey = new CompositeKey();
//        compositeKey.setHotelId(hotelId);
//        compositeKey.setSrchCI(srchCI);
//        compositeKey.setId(id);
//        int expectedPartition = compositeKey.getHotelId().hashCode() % 3;
//        NaturalKeyPartitioner partitioner = new NaturalKeyPartitioner();
//        int partition = partitioner.getPartition(compositeKey, NullWritable.get(), 3);
//        assertThat(expectedPartition, is(partition));
//    }
//}
