//package com.epam.secondsort;
//
//import org.apache.hadoop.io.LongWritable;
//import org.apache.hadoop.io.NullWritable;
//import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mrunit.mapreduce.MapDriver;
//import org.junit.Before;
//import org.junit.Test;
//
///**
// * User: Ihor Kovalyk
// * Date: 9/12/19
// * Time: 12:05 AM
// */
//public class MyMapperTest {
//    final String input = "18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,5,2017-09-19,2017-09-20,2,0,1,8243,1,2826088480774";
//    MapDriver<Object, Text, CompositeKey, NullWritable> mapDriver;
//    private Long id = 18L;
//    private String dateTime = "2015-08-18 12:37:10";
//    private Integer siteName = 2;
//    private Integer posaContinent = 3;
//    private Integer userLocationCountry = 57;
//    private Integer userLocationRegion = 342;
//    private Integer userLocationCity = 5021;
//    private Double origDestinationDistance = null;
//    private Integer userId = 57;
//    private Integer isMobile = 0;
//    private Integer isPackage = 0;
//    private Integer channel = 5;
//    private String srchCI = "2017-09-19";
//    private String srchCO = "2017-09-20";
//    private Integer srchAdultsCount = 2;
//    private Integer srchChildrenCount = 0;
//    private Integer srchRmCount = 1;
//    private Integer srchDestinationId = 8243;
//    private Integer srchDestinationTypeId = 1;
//    private Long hotelId = 2826088480774L;
//
//
//    @Before
//    public void setUp() {
//        MyMapper mapper = new MyMapper();
//        mapDriver = MapDriver.newMapDriver();
//        mapDriver.setMapper(mapper);
//    }
//
//    /**
//     * @throws Exception
//     */
//    @Test
//    public void testMapper() throws Exception {
//        mapDriver.withInput(new LongWritable(), new Text(input));
//        mapDriver.withOutput(new CompositeKey(id, dateTime, siteName, posaContinent, userLocationCountry,
//                userLocationRegion, userLocationCity, origDestinationDistance,
//                userId, isMobile, isPackage, channel, srchCI, srchCO,
//                srchAdultsCount, srchChildrenCount, srchRmCount, srchDestinationId, srchDestinationTypeId,
//                hotelId), NullWritable.get());
//
//        mapDriver.runTest();
//    }
//}
//
//
