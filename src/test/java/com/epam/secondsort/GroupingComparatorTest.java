//package com.epam.secondsort;
//
//import org.junit.Test;
//
//import static org.junit.Assert.assertThat;
//import static org.hamcrest.CoreMatchers.*;
//
///**
// * User: Ihor Kovalyk
// * Date: 9/12/19
// * Time: 10:11 AM
// */
//
//public class GroupingComparatorTest {
//    private CompositeKey compositeKey = new CompositeKey(28260883L, "2017-09-19", 19L);
//    private CompositeKey compositeKey2 = new CompositeKey(28260884L,"2017-09-20",20L);
//    private GroupingComparator comparator = new GroupingComparator();
//
//    @Test
//    public void testCompare() throws Exception {
//        assertThat(comparator.compare(compositeKey,compositeKey2),is(-1));
//    }
//
//    @Test
//    public void testCompareGreater() throws Exception {
//        assertThat(comparator.compare(compositeKey2,compositeKey),is(1));
//    }
//
//    @Test
//    public void testCompareEquals() throws Exception {
//        assertThat(comparator.compare(compositeKey2,compositeKey2),is(0));
//    }
//}
