//package com.epam.secondsort;
//
//import org.apache.hadoop.io.NullWritable;
//import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * User: Ihor Kovalyk
// * Date: 9/12/19
// * Time: 11:35 AM
// */
//public class MyReducerTest {
//    @Test
//    public void testReducerCold() throws Exception {
//        final NullWritable nullWritable = NullWritable.get();
//        final CompositeKey compositeKey = new CompositeKey(282605467L, "2015-08-18 12:37:10", 18,
//                25, 5, 6, 57, 348D,
//                5029, 2, 57, 0, "2017-09-19", "2017-09-20",
//                6, 0, 1, 8243, 2,5L);
//        List<NullWritable> list = new ArrayList<NullWritable>();
//        list.add(NullWritable.get());
//        new ReduceDriver<CompositeKey, NullWritable, CompositeKey, NullWritable>()
//                .withReducer(new MyReducer())
//                .withInput(compositeKey, list)
//                .withOutput(compositeKey, nullWritable)
//                .runTest();
//
//    }
//}
