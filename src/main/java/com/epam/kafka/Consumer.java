package com.epam.kafka;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.FlatMapGroupsWithStateFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.GroupStateTimeout;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.elasticsearch.hadoop.cfg.ConfigurationOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.split;

public class Consumer {

    public static void main(String[] args) throws StreamingQueryException, InterruptedException {
        Logger.getLogger("org").setLevel(Level.WARN);
        SparkSession sparkSession = SparkSession.builder()
                .config(ConfigurationOptions.ES_NET_HTTP_AUTH_USER, "elastic")
                .config(ConfigurationOptions.ES_NET_HTTP_AUTH_PASS, "somepass")
                .config(ConfigurationOptions.ES_NODES, "localhost")
                .config(ConfigurationOptions.ES_PORT, "9200")
                .appName("Structured_Streaming")
                .master("local[*]")
                .getOrCreate();

        Dataset<Event> eventDataset = sparkSession.readStream()
                .format("kafka").option("kafka.bootstrap.servers", "localhost:9092")
                .option("subscribe", "first_topic")
                .load()
                .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
                .withColumn("temp", split(col("key"), " "))
                .select(col("temp").getItem(0).as("name"),
                        col("temp").getItem(1).as("activity"),
                        col("value").cast("timestamp").as("start"))
                .drop("temp")
                .as(Encoders.bean(Event.class))
                .withWatermark("start", "5 minutes")
                .dropDuplicates();

        Dataset<EventUpdate> eventUpdateDataset = eventDataset
                .groupByKey((MapFunction<Event, String>) Event::getName, Encoders.STRING())
                .flatMapGroupsWithState(getUpdatedEvent(), OutputMode.Append(), Encoders.bean(EventUpdate.class),
                        Encoders.bean(EventUpdate.class),
                        GroupStateTimeout.EventTimeTimeout());

        Dataset<Row> countEvent = eventDataset.distinct().groupBy(functions.window(eventDataset.col("start"),
                "1 minutes", "1 minutes"),
                eventDataset.col("name")).count();

        eventUpdateDataset.writeStream()
                .outputMode(OutputMode.Append())
                .format("console")
                .option("truncate", false)
                .format("es")
                .option("checkpointLocation", "/tmp/stream1_checkpoint")
                .trigger(Trigger.ProcessingTime(5, TimeUnit.SECONDS))
                .start("index/type1");

        countEvent.writeStream()
                .outputMode("complete")
                .format("console")
                .option("truncate", false)
                .start()
                .awaitTermination();
    }

    public static FlatMapGroupsWithStateFunction<String, Event, EventUpdate, EventUpdate> getUpdatedEvent() {
        FlatMapGroupsWithStateFunction<String, Event, EventUpdate, EventUpdate> stateFunction = (s, iterator, groupState) -> {
            List<EventUpdate> list = new ArrayList<>();
            Event event = iterator.next();
            EventUpdate userData = new EventUpdate(event.getName(), event.getActivity(),
                    event.getStart(), null);
            if (groupState.exists()) {
                EventUpdate userData1 = groupState.get();
                if (userData1.getActivity().equals(userData.getActivity())) {
                    list.add(userData1);
                    return list.iterator();
                }
                userData1.setEnd(event.getStart());
                groupState.update(userData);
                list.add(userData);
                list.add(userData1);
                return list.iterator();
            } else {
                groupState.update(userData);
                list.add(userData);
                return list.iterator();
            }
        };
        return stateFunction;
    }
}
