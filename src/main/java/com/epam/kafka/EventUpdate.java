package com.epam.kafka;

import java.io.Serializable;
import java.sql.Timestamp;

public class EventUpdate implements Serializable {
    String name;
    String activity;
    Timestamp start;
    Timestamp end;

    public EventUpdate() {
    }

    public EventUpdate(String name, String activity, Timestamp start, Timestamp end) {
        this.name = name;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

}
