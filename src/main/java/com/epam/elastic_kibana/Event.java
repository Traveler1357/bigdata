package com.epam.elastic_kibana;

import java.io.Serializable;
import java.sql.Timestamp;

public class Event implements Serializable {
    private String name;
    private String activity;
    private Timestamp start;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }
}
