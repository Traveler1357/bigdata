package com.epam.elastic_kibana;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.sql.Timestamp;
import java.util.Properties;
import java.util.Scanner;

public class Producer {

    public static void main(String[] args) {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<>(props);

        Scanner input = new Scanner(System.in);
        while (input.hasNext()) {
            String key = input.nextLine();
            Timestamp value = new Timestamp(System.currentTimeMillis());
            producer.send(new ProducerRecord("first_topic",key, value.toString()));
        }

        input.close();
        producer.close();
    }
}

