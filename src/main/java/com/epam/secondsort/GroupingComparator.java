package com.epam.secondsort;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * At the grouping stage, all of the records are already in secondary-sort
 * order, and the GroupingComparator bundles together records with the same
 * natural key ('hotelId').
 *
 * @author Ihor Kovalyk
 */
public class GroupingComparator extends WritableComparator {
    protected GroupingComparator() {
        super(CompositeKey.class, true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        CompositeKey compositeKeyA = (CompositeKey) a;
        CompositeKey compositeKeyB = (CompositeKey) b;

        return compositeKeyA.getHotelId().compareTo(compositeKeyB.getHotelId());
    }
}
