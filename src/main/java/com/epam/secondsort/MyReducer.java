package com.epam.secondsort;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * This reducer will fetch the partitions (from different mappers) and then sort them by ('hotelId', 'srchCI', 'id')
 * order again, because we used our CompositeKeyComparator as the sort comparator class.
 * After that, it will group all sorted partition data by natural key ('hotelId') because we used our GroupingComparator
 * as the grouping comparator.
 * These groups are passed to the "reduce" function in order or natural key, and their content is
 * sorted in order of secondary key.
 * So the output of the reducer will be rows ordered by ('hotelId', 'srchCI', 'id').
 *
 * @author Ihor Kovalyk
 *
 */
public class MyReducer extends Reducer<CompositeKey, NullWritable, CompositeKey, NullWritable> {
    @Override
    protected void reduce(CompositeKey key, Iterable<NullWritable> nullWriteable, Context context)
            throws IOException, InterruptedException {
        context.write(key, NullWritable.get());
    }
}
