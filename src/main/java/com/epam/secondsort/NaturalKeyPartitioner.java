package com.epam.secondsort;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * The partitioner calculates a hash based on the natural key ('hotelId') in the CompositeKey class, and
 * performs a modulo of that with the number of partitions (which is the number of
 * reducers) to determine which reducer should receive a map output record.
 *
 * @author Ihor Kovalyk
 */

public class NaturalKeyPartitioner extends Partitioner<CompositeKey, NullWritable> {

    @Override
    public int getPartition(CompositeKey compositeKey, NullWritable values, int numPartitions) {

        return compositeKey.getHotelId().hashCode() % numPartitions;
    }
}

