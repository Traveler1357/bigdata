package com.epam.spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class SparkDS {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        final SparkSession session = SparkSession.builder()
                .appName("TopTen")
                .config("key", "value")
                .master("local")
                .getOrCreate();

        final Dataset<Row> datasetGoalies = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/Goalies.csv")
                .withColumnRenamed("playerID", "goalies_playerID")
                .withColumnRenamed("GA", "goaliesGA");

        final Dataset<Row> datasetTeam = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/Teams.csv")
                .withColumnRenamed("name", "team_name");

        final Dataset<Row> datasetAward = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/AwardsPlayers.csv")
                .withColumnRenamed("playerID", "award_playerID");

        final Dataset<Row> datasetMaster = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/Master.csv")
                .withColumn("fullname", concat(col("firstName"), col("lastName")))
                .select(col("playerID"), col("fullname"));

        /** Retrieving columns playerID and award from Awards table and doing aggregation
         * operation 'count()' to count the amount of the awards for each player.*/
        final Dataset<Row> awards = datasetAward.select("award_playerID", "award")
                .groupBy("award_playerID").agg(count("award").as("award"));

        /** Retrieving columns playerID and goals from Goalies table and doing aggregation
         * operation 'sum()' to get the sum of goals scored by each player.*/
        final Dataset<Row> topGoals = datasetGoalies.select("goalies_playerID", "goaliesGA")
                .groupBy("goalies_playerID").agg(sum("goaliesGA").as("goals"))
                .orderBy(desc("goals"));

        /** Joining tables Goalies and Teams using the column teamID.*/
        final Dataset<Row> playerAndTeam = datasetGoalies.join(datasetTeam,
                datasetGoalies.col("teamID").equalTo(datasetTeam.col("tmID")))
                .select(col("goalies_playerID").as("team_playerID"), col("team_name"));

        /** Appending teams as a list to playerID.*/
        final Dataset<Row> resultPlayerAndTeam = playerAndTeam.distinct().groupBy("team_playerID")
                .agg(collect_list("team_name").as("teams")).coalesce(1);

        /** Creating the table by joining all tables and using the key playerID.*/
        final Dataset<Row> joinedColumns = datasetMaster.join(awards,
                datasetMaster.col("playerID").equalTo(awards.col("award_playerID")))
                .join(resultPlayerAndTeam, datasetMaster.col("playerID")
                        .equalTo(resultPlayerAndTeam.col("team_playerID")))
                .join(topGoals, datasetMaster.col("playerID").equalTo(topGoals.col("goalies_playerID")));

        /**Getting top 10 players by scored goals.*/
        final Dataset<Row> topTen = joinedColumns.select(col("fullname"), col("award"),
                col("goals"), col("teams")).orderBy(desc("goals")).limit(10);

        topTen.write().format("json").save("resultDataset_JSON");
        session.stop();
    }
}
