package com.epam.spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.concat;

public class SparkSQL {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        final SparkSession session = SparkSession.builder()
                .appName("PlayerAndTeam")
                .config("key", "value")
                .master("local")
                .getOrCreate();

        final Dataset<Row> datasetGoalies = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/Goalies.csv")
                .withColumnRenamed("GA", "goaliesGA");

        final Dataset<Row> datasetTeam = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/Teams.csv")
                .withColumnRenamed("name", "team_name");

        final Dataset<Row> datasetAward = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/AwardsPlayers.csv");

        final Dataset<Row> datasetMaster = session.read()
                .option("inferSchema", "true")
                .option("header", "true")
                .csv("src/main/resources/Master.csv")
                .withColumn("fullname", concat(col("firstName"), col("lastName")));

        datasetAward.createOrReplaceTempView("award_table");
        datasetGoalies.createOrReplaceTempView("goalies_table");
        datasetTeam.createOrReplaceTempView("team_table");
        datasetMaster.createOrReplaceTempView("master_table");

        final Dataset<Row> countGoals = session.sql("SELECT MIN(g.teamID) AS teamID, g.playerID, " +
                "SUM(g.goaliesGA) AS goals FROM goalies_table g GROUP BY g.playerID ORDER BY goals DESC");
        countGoals.createOrReplaceTempView("goals");

        final Dataset<Row> countAward = session.sql("SELECT m.playerID, m.fullname, COUNT(a.award) AS award " +
                "FROM award_table a JOIN master_table m ON m.playerID = a.playerID " +
                " GROUP BY m.playerID, m.fullname HAVING award ");
        countAward.createOrReplaceTempView("awards");

        final Dataset<Row> joinTwo = session.sql("SELECT a.playerID, a.fullname, a.award, g.goals, g.teamID " +
                "FROM awards a JOIN goals g ON a.playerID = g.playerID order by g.goals DESC");
        joinTwo.createOrReplaceTempView("joined");

        final Dataset<Row> result = session.sql("SELECT j.fullname, j.award, j.goals," +
                " collect_list(distinct t.team_name) AS team  FROM joined j JOIN team_table t ON " +
                "j.teamID = t.tmID group by j.fullname, j.award, j.goals LIMIT 10");

        result.write().format("parquet").save("resultSQL");
        result.write().format("json").save("resultSQL_JSON");
        session.stop();
    }
}
