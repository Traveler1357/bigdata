package com.epam.spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.List;

public class SparkRDD {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        final SparkConf conf = new SparkConf()
                .setAppName("CountGoals")
                .setMaster("local[*]");
        final JavaSparkContext sparkContext = new JavaSparkContext(conf);
        final JavaRDD<String> goalsData =
                sparkContext.textFile("src/main/resources/Goalies.csv");
        final JavaRDD<String> awardsData =
                sparkContext.textFile("src/main/resources/AwardsPlayers.csv");
        final JavaRDD<String> masterData =
                sparkContext.textFile("src/main/resources/Master.csv");
        final JavaRDD<String> teamsData =
                sparkContext.textFile("src/main/resources/Teams.csv");

        /** Getting the first row, which is the header of the file.*/
        String goalsHeader = goalsData.first();
        String teamsHeader = teamsData.first();
        String awardsHeader = awardsData.first();
        String masterHeader = masterData.first();

        /** Filtering JavaRDD to omit the header when reading the file.*/
        final JavaRDD<String> filteredGoalsRows = goalsData.filter(row -> !row.equals(goalsHeader));
        final JavaRDD<String> filteredTeamsRows = teamsData.filter(row -> !row.equals(teamsHeader));
        final JavaRDD<String> filteredAwardsRows = awardsData.filter(row -> !row.equals(awardsHeader));
        final JavaRDD<String> filteredMasterRows = masterData.filter(row -> !row.equals(masterHeader));

        /** Creating a key value RDD.
         * @value playerId used as a key.
         * @value goals used as a value.*/
        final JavaPairRDD<String, Integer> onlyGoals = filteredGoalsRows.mapToPair(line -> {
            String[] values = line.split(",");
            String playerId = values[0];
            String goals = values[12];
            if (goals.equals("")) {
                return new Tuple2<>(playerId, Integer.parseInt("0"));
            }
            return new Tuple2<>(playerId, Integer.parseInt(goals));
        });

        /** Calculating goals for each player.*/
        final JavaPairRDD<String, Integer> goalsSum = onlyGoals.reduceByKey((a, b) -> a + b);

        /** Creating a key value RDD from table Goalies.
         * @value teamId used as a key.
         * @value playerId used as a value.*/
        final JavaPairRDD<String, String> playerIDAndTeamId = filteredGoalsRows.mapToPair(line -> {
            String[] values = line.split(",", -1);
            String teamId = values[3];
            String playerId = values[0];
            return new Tuple2<>(teamId, String.join("", playerId));
        });

        /** Creating a key value RDD from table Teams.
         * @value teamID used as a key.
         * @value teamName used as a value.*/
        final JavaPairRDD<String, String> teamIdAndTeamName = filteredTeamsRows.mapToPair(line -> {
            String[] values = line.split(",", -1);
            String teamId = values[2];
            String teamName = values[18];
            return new Tuple2<>(teamId, String.join("", teamName));
        });

        /**Getting column 'playerID' as the key from table AwardsPlayers*/
        JavaPairRDD<String, Long> onlyPlayersIDAndAward = filteredAwardsRows.mapToPair(line -> {
            String[] values = line.split(",");
            return new Tuple2<>(values[0], 1L);
        });
        JavaPairRDD<String, Long> countedAward = onlyPlayersIDAndAward.reduceByKey((a, b) -> a + b);// counted awards

        /** Creating a key value RDD from table Master.
         * @value teamID used as a key.
         * @value firstName used as a value
         * @value lastName used as a value to create fullName.*/
        JavaPairRDD<String, String> onlyPlayersIdAndFullName = filteredMasterRows.mapToPair(line -> {
            String[] values = line.split(",");
            String playerId = values[0];
            String firstName = values[3];
            String lastName = values[4];
            return new Tuple2<>(playerId, String.join(" ", firstName, lastName));
        });

        /**Creating a RDD with 'playerID', player's fullName and counted number of awards by joining two RDDs.*/
        final JavaPairRDD<String, Tuple2<String, Long>> joined = onlyPlayersIdAndFullName.join(countedAward);

        /** Joining two RDDs TeamName and PlayerId by keys.*/
        final JavaPairRDD<String, Tuple2<String, String>> joinedTeamAndPlayer = playerIDAndTeamId.join(teamIdAndTeamName);

        final JavaPairRDD<String, Iterable<String>> withoutTeamId = joinedTeamAndPlayer
                .mapToPair(line -> new Tuple2<>(line._2._1, line._2._2)).distinct().groupByKey();

        /** Created a RDD with values playerID, fullName, awards, goals and teams by joining previous RDDs.*/
        final JavaPairRDD<String, Tuple2<Tuple2<Tuple2<String, Long>,
                Integer>, Iterable<String>>> joinedNameAwardGoalsTeam = joined
                .join(goalsSum)
                .join(withoutTeamId);

        /** Creating a RDD without playerID and put goals as a key for sorting in descending order.*/
        final JavaPairRDD<Integer, Tuple2<Tuple2<String, Long>,
                Iterable<String>>> joinedWithoutPlayerID = joinedNameAwardGoalsTeam.mapToPair(line ->
                (new Tuple2<>(line._2._1._2, new Tuple2<>(
                        new Tuple2<>(line._2._1._1._1, line._2._1._1._2), line._2._2)))).sortByKey(false);

        /**Creating a RDD with values fullName, award, goals and teams as was demanded in the task.*/
        final JavaPairRDD<String, String> sortedByGoals = joinedWithoutPlayerID.mapToPair(line ->
                (new Tuple2<>(line._2._1._1, String.join(",", line._2._1._2.toString(),
                        line._1.toString(), line._2._2.toString()))));
        System.out.println(sortedByGoals.take(5));

        /** Getting the first 10 results and put those results in the list. */
        final List<Tuple2<String, String>> list = sortedByGoals.take(10);

        final JavaRDD<Tuple2<String, String>> output = sparkContext.parallelize(list);
        output.coalesce(1).saveAsTextFile("outputTop10_RDD.csv");

        sparkContext.stop();
    }
}
