package com.epam.spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class Cluster {
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        final SparkSession session = SparkSession.builder()
                .appName("TopTen")
                .config("key", "value")
                .getOrCreate();

        final Dataset<Row> datasetAward = session.read()
                .format("csv")
                .option("inferSchema", "true")
                .option("header", "true")
                .load(args[0])
                .withColumnRenamed("playerID", "award_playerID");

        final Dataset<Row> datasetGoalies = session.read()
                .format("csv")
                .option("inferSchema", "true")
                .option("header", "true")
                .load(args[1])
                .withColumnRenamed("playerID", "goalies_playerID")
                .withColumnRenamed("GA", "goaliesGA");

        final Dataset<Row> datasetTeam = session.read()
                .format("csv")
                .option("inferSchema", "true")
                .option("header", "true")
                .load(args[2])
                .withColumnRenamed("name", "team_name");

        final Dataset<Row> datasetMaster = session.read()
                .format("csv")
                .option("inferSchema", "true")
                .option("header", "true")
                .load(args[3])
                .withColumn("fullname", concat(col("firstName"), col("lastName")))
                .select(col("playerID"), col("fullname"));

        final Dataset<Row> awards = datasetAward.select("award_playerID", "award")
                .groupBy("award_playerID").agg(count("award").as("award"));

        final Dataset<Row> topGoals = datasetGoalies.select("goalies_playerID", "goaliesGA")
                .groupBy("goalies_playerID").agg(sum("goaliesGA").as("goals"))
                .orderBy(desc("goals"));

        final Dataset<Row> playerAndTeam = datasetGoalies.join(datasetTeam,
                datasetGoalies.col("teamID").equalTo(datasetTeam.col("tmID")))
                .select(col("goalies_playerID").as("team_playerID"), col("team_name"));

        final Dataset<Row> resultPlayerAndTeam = playerAndTeam.distinct().groupBy("team_playerID")
                .agg(collect_list("team_name").as("teams")).coalesce(1);

        final Dataset<Row> joinedColumns = datasetMaster.join(awards,
                datasetMaster.col("playerID").equalTo(awards.col("award_playerID")))
                .join(resultPlayerAndTeam, datasetMaster.col("playerID")
                        .equalTo(resultPlayerAndTeam.col("team_playerID")))
                .join(topGoals, datasetMaster.col("playerID").equalTo(topGoals.col("goalies_playerID")));

        final Dataset<Row> topTen = joinedColumns.select(col("fullname"), col("award"),
                col("goals"), col("teams")).orderBy(desc("goals")).limit(10);

        topTen.write().format("json").save("result");

        session.stop();
    }
}
